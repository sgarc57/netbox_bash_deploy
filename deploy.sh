#!/bin/bash
#
yum install -y postgresql postgresql-server postgresql-devel python-psycopg2 \
gcc python3 python3-devel python3-pip libxml2-devel libxslt-devel libffi-devel graphviz openssl-devel \
wget git
#
postgresql-setup initdb
#
sed -i 's/ident/md5/g' /var/lib/pgsql/data/pg_hba.conf
#
cat <<EOF | sudo -u postgres psql
CREATE DATABASE netbox;
CREATE USER netbox WITH PASSWORD 'labnetbox321';
GRANT ALL PRIVILEGES ON DATABASE netbox TO netbox;
\q
EOF
#
wget https://bootstrap.pypa.io/get-pip.py
#
python get-pip.py
#
mkdir -p /opt/netbox && cd /opt/netbox/
#
git clone -b master https://github.com/digitalocean/netbox.git
#
cd /opt/netbox/netbox/
#
pip install -r requirements.txt
#
cp ./netbox/netbox/configuration.example.py ./netbox/netbox/configuration.py
#
SECRET_KEY=$(python netbox/generate_secret_key.py)
sed -i s#SECRET_KEY = ''#SECRET_KEY = '$SECRET_KEY'#g ./netbox/netbox/configuration.py
#
python ./netbox/manage.py migrate
python ./netbox/manage.py createsuperuser
python ./netbox/manage.py collectstatic
python ./netbox/manage.py loaddata initial_data
#
(python ./netbox/manage.py runserver 0.0.0.0:80 --insecure &)
#
firewall-cmd --zone=public --add-port=80/tcp --permanent
firewall-cmd --reload

